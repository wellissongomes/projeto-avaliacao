package com.memory.medicamentos.services;

import com.memory.medicamentos.exceptions.NotFoundException;
import com.memory.medicamentos.models.Drug;
import com.memory.medicamentos.repositories.DrugRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DrugServiceTest {
    @Mock
    private DrugRepository drugRepository;

    private DrugService drugService;
    private PodamFactory podamFactory;

    @BeforeEach
    void setUp() {
        podamFactory = new PodamFactoryImpl();
        drugService = new DrugServiceImpl(drugRepository);
    }
    @Test
    @DisplayName("Test saving drug")
    public void saveDrug() {
        Drug drug = podamFactory.manufacturePojo(Drug.class);

        when(drugRepository.save(drug)).thenReturn(drug);

        Drug drugResult = drugService.save(drug);

        assertEquals(drug, drugResult);
    }

    @Test
    @DisplayName("Test get all drugs")
    public void testGetAll() {
        List<Drug> drugs = podamFactory.manufacturePojo(ArrayList.class, Drug.class);

        Pageable pageable = PageRequest.of(0, 10);
        when(drugRepository.findAll(eq(pageable))).thenReturn(new PageImpl<>(drugs, pageable, drugs.size()));

        Page<Drug> returnedDrugs = drugService.getAll(pageable);

        assertEquals(drugs, returnedDrugs.getContent());
        assertEquals(drugs.size(), returnedDrugs.getContent().size());
    }

    @Test
    @DisplayName("Test update specific drug")
    public void testUpdate() {
        Drug drug = podamFactory.manufacturePojo(Drug.class);
        Drug drugToUpdate = podamFactory.manufacturePojo(Drug.class);
        Long drugToUpdateId = 1L;

        when(drugRepository.findById(eq(drugToUpdateId))).thenReturn(Optional.of(drugToUpdate));
        when(drugRepository.save(any(Drug.class))).thenReturn(drug);

        Drug updatedDrug = drugService.update(drug, drugToUpdateId);

        assertEquals(drug, updatedDrug);
    }

    @Test
    @DisplayName("Test try to update drug does not exists")
    public void testUpdateWithWrongId() {
        Drug drug = podamFactory.manufacturePojo(Drug.class);
        Long drugToUpdateId = 1L;

        when(drugRepository.findById(eq(drugToUpdateId))).thenThrow(new NotFoundException());

        assertThrows(NotFoundException.class, () -> {
            drugService.update(drug, drugToUpdateId);
        });
    }

    @Test
    @DisplayName("Test delete specific drug")
    public void testDelete() {
        Drug mockDrug = podamFactory.manufacturePojo(Drug.class);
        Long drugId = mockDrug.getId();

        when(drugRepository.findById(eq(drugId))).thenReturn(Optional.of(mockDrug));
        doNothing().when(drugRepository).deleteById(drugId);

        drugService.delete(drugId);

        verify(drugRepository, times(1)).deleteById(eq(drugId));
    }

    @Test
    @DisplayName("Test get all drugs by name")
    public void testGetAllByName() {
        String name = "Ibuprofeno";
        Pageable paging = PageRequest.of(0, 10);

        Drug mockDrug = podamFactory.manufacturePojo(Drug.class);

        Page<Drug> expectedDrugs = new PageImpl<>(Collections.singletonList(mockDrug));
        when(drugRepository.findByName(name, paging)).thenReturn(expectedDrugs);

        Page<Drug> returnedDrugs = drugService.getAllByName(name, paging);

        assertEquals(expectedDrugs, returnedDrugs);
    }


    @Test
    @DisplayName("Test get all drugs by anvisa number")
    public void testGetAllByAnvisaNumber() {
        String anvisaNumber = "12345678";
        Pageable paging = PageRequest.of(0, 10);

        Drug mockDrug = podamFactory.manufacturePojo(Drug.class);

        Page<Drug> expectedDrugs = new PageImpl<>(Collections.singletonList(mockDrug));
        when(drugRepository.findByAnvisaNumber(anvisaNumber, paging)).thenReturn(expectedDrugs);

        Page<Drug> returnedDrugs = drugService.getAllByAnvisaNumber(anvisaNumber, paging);

        assertEquals(expectedDrugs, returnedDrugs);
    }

}