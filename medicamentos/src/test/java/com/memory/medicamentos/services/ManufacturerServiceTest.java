package com.memory.medicamentos.services;

import com.memory.medicamentos.exceptions.NotFoundException;
import com.memory.medicamentos.models.Manufacturer;
import com.memory.medicamentos.repositories.ManufacturerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ManufacturerServiceTest {
    @Mock
    private ManufacturerRepository manufacturerRepository;

    private ManufacturerService manufacturerService;
    private PodamFactory podamFactory;

    @BeforeEach
    void setUp() {
        podamFactory = new PodamFactoryImpl();
        manufacturerService = new ManufacturerServiceImpl(manufacturerRepository);
    }

    @Test
    @DisplayName("Test get by id with specific id")
    void testGetById() {
        Manufacturer expectedManufacturer = podamFactory.manufacturePojo(Manufacturer.class);
        Long manufacturerId = expectedManufacturer.getId();

        when(manufacturerRepository.findById(eq(manufacturerId))).thenReturn(Optional.of(expectedManufacturer));

        Manufacturer actualManufacturer = manufacturerService.getById(manufacturerId);

        assertEquals(expectedManufacturer, actualManufacturer);
    }

    @Test
    void testGetByIdWrongId() {
        Long manufacturerId = 1L;
        when(manufacturerRepository.findById(eq(manufacturerId))).thenThrow(new NotFoundException());

        assertThrows(NotFoundException.class, () -> manufacturerService.getById(manufacturerId));
    }

    @Test
    void testGetAll() {
        Pageable paging = PageRequest.of(0, 10);
        List<Manufacturer> expectedManufacturers = podamFactory.manufacturePojo(ArrayList.class, Manufacturer.class);
        Page<Manufacturer> expectedPage = new PageImpl<>(expectedManufacturers, paging, expectedManufacturers.size());
        when(manufacturerRepository.findAll(paging)).thenReturn(expectedPage);

        Page<Manufacturer> actualPage = manufacturerService.getAll(paging);

        assertEquals(expectedPage, actualPage);
    }
}