package com.memory.medicamentos.repositories;

import com.memory.medicamentos.models.Drug;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DrugRepository extends JpaRepository<Drug, Long> {
    @Modifying
    @Query(value = "DELETE FROM tb_drugs_adverse_reactions WHERE adverse_reaction_id = :adverseReactionId", nativeQuery = true)
    int removeAdverseReactionId(@Param("adverseReactionId") Long adverseReactionId);

    Page<Drug> findByName(String name, Pageable paging);

    Page<Drug> findByAnvisaNumber(String anvisaNumber, Pageable paging);
}
