package com.memory.medicamentos.controllers;

import com.memory.medicamentos.dtos.DrugResponse;
import com.memory.medicamentos.dtos.PageResponse;
import com.memory.medicamentos.models.Manufacturer;
import com.memory.medicamentos.services.ManufacturerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/drugs/manufacturers")
public class ManufacturerController {
    private final ManufacturerService manufacturerService;

    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public ResponseEntity<PageResponse<DrugResponse>> getAllAdverseReactionsFromIds(@RequestParam(defaultValue = "0") int page,
                                                                                    @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Manufacturer> manufacturers = manufacturerService.getAll(paging);
        return new ResponseEntity<>(new PageResponse(manufacturers), HttpStatus.OK);
    }
}
