package com.memory.medicamentos.controllers;

import com.memory.medicamentos.dtos.DrugMapper;
import com.memory.medicamentos.dtos.DrugRequest;
import com.memory.medicamentos.dtos.DrugResponse;
import com.memory.medicamentos.dtos.PageResponse;
import com.memory.medicamentos.models.Drug;
import com.memory.medicamentos.services.DrugService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/drugs")
public class DrugController {

    private final DrugMapper drugMapper;
    private final DrugService drugService;

    public DrugController(DrugMapper drugMapper, DrugService drugService) {
        this.drugMapper = drugMapper;
        this.drugService = drugService;
    }

    @GetMapping
    public ResponseEntity<PageResponse<DrugResponse>> getAlldrugs(@RequestParam(defaultValue = "0") int page,
                                                                                    @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<DrugResponse> drugsPage = drugService.getAll(paging).map(drugMapper::toDrugResponse);
        return new ResponseEntity<>(new PageResponse(drugsPage), HttpStatus.OK);
    }

    @GetMapping("/name")
    public ResponseEntity<PageResponse<DrugResponse>> getDrugsByName(@RequestParam String name,
                                                                     @RequestParam(defaultValue = "0") int page,
                                                                     @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<DrugResponse> drugsPage = drugService.getAllByName(name, paging).map(drugMapper::toDrugResponse);
        return new ResponseEntity<>(new PageResponse(drugsPage), HttpStatus.OK);
    }

    @GetMapping("/anvisa-number")
    public ResponseEntity<PageResponse<DrugResponse>> getDrugsByAnvisaNumber(@RequestParam String anvisaNumber,
                                                                     @RequestParam(defaultValue = "0") int page,
                                                                     @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<DrugResponse> drugsPage = drugService.getAllByAnvisaNumber(anvisaNumber, paging).map(drugMapper::toDrugResponse);
        return new ResponseEntity<>(new PageResponse(drugsPage), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DrugResponse> addDrug(@RequestBody DrugRequest drugRequest) {
        Drug drug = drugMapper.toDrug(drugRequest);
        Drug savedDrug = drugService.save(drug);
        DrugResponse drugResponse = drugMapper.toDrugResponse(savedDrug);
        return new ResponseEntity<>(drugResponse, HttpStatus.CREATED);
    }

    @PutMapping("/{drugId}")
    public ResponseEntity<DrugResponse> updateDrug(@RequestBody DrugRequest drugRequest, @PathVariable Long drugId) {
        Drug drug = drugMapper.toDrug(drugRequest);
        Drug updatedDrug = drugService.update(drug, drugId);
        DrugResponse drugResponse = drugMapper.toDrugResponse(updatedDrug);
        return new ResponseEntity<>(drugResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{drugId}")
    public ResponseEntity<Void> deleteDrug(@PathVariable Long drugId) {
        drugService.delete(drugId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
