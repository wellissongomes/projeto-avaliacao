package com.memory.medicamentos.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class DrugRequest {
    private final String anvisaNumber;
    private final String name;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private final String expirationDate;
    private final String price;
    private final String telSAC;
    private final Long manufacturerId;
    private final Set<Long> adverseReactionIds;
}
