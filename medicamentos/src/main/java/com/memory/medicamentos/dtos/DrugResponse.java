package com.memory.medicamentos.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.memory.medicamentos.models.Manufacturer;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
@Builder
public class DrugResponse {
    private final Long id;
    private final String anvisaNumber;
    private final String name;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private final Date expirationDate;
    private final String price;
    private final String telSAC;
    private final Manufacturer manufacturer;
    private final Set<Long> adverseReactionIds;
}
