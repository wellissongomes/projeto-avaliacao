package com.memory.medicamentos.dtos;

import com.memory.medicamentos.exceptions.BadRequestException;
import com.memory.medicamentos.models.Drug;
import com.memory.medicamentos.services.ManufacturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class DrugMapper {
    private final ManufacturerService manufacturerService;

    public DrugMapper(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public Drug toDrug(DrugRequest drugRequest) {
        var manufacturer = manufacturerService.getById(drugRequest.getManufacturerId());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        decimalFormat.setParseBigDecimal(true);

        verifyAllRequiredPatterns(drugRequest);

        try {
            return Drug.builder()
                    .anvisaNumber(drugRequest.getAnvisaNumber())
                    .price((BigDecimal) decimalFormat.parse(drugRequest.getPrice()))
                    .adverseReactionIds(drugRequest.getAdverseReactionIds())
                    .expirationDate(formatter.parse(drugRequest.getExpirationDate()))
                    .manufacturer(manufacturer)
                    .telSAC(drugRequest.getTelSAC())
                    .name(drugRequest.getName())
                    .build();
        } catch (ParseException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    public DrugResponse toDrugResponse(Drug drug) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");

        return DrugResponse.builder()
                .anvisaNumber(drug.getAnvisaNumber())
                .id(drug.getId())
                .adverseReactionIds(drug.getAdverseReactionIds())
                .telSAC(drug.getTelSAC())
                .price(decimalFormat.format(drug.getPrice()))
                .expirationDate(drug.getExpirationDate())
                .manufacturer(drug.getManufacturer())
                .name(drug.getName())
                .build();
    }

    private void verifyAllRequiredPatterns(DrugRequest drugRequest) {
        verifyAnvisaCodePattern(drugRequest.getAnvisaNumber());
        verifyTelSACPattern(drugRequest.getTelSAC());
        verifyDatePattern(drugRequest.getExpirationDate());
    }

    private void verifyDatePattern(String expirationDate) {
        String regex = "^\\d{2}/\\d{2}/\\d{4}$";
        verifyPattern(regex, expirationDate, "dd/MM/yyyy");
    }

    private void verifyTelSACPattern(String telSAC) {
        String regex = "^\\(\\d{2}\\) \\d{1} \\d{4}-\\d{4}$";
        verifyPattern(regex, telSAC, "(00) 0 0000-0000");
    }

    private void verifyAnvisaCodePattern(String anvisaNumber) {
        String regex = "^\\d{1}\\.\\d{4}\\.\\d{4}\\.\\d{3}-\\d{1}$";
        verifyPattern(regex, anvisaNumber, "0.0000.0000.000-0");
    }

    private void verifyPattern(String regex, String input, String format) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (!matcher.matches()) {
            log.error("[{}] invalid format. Required: [{}]", input, format);
            throw new BadRequestException("Format required: " + format);
        }
    }
}
