package com.memory.medicamentos.services;

import com.memory.medicamentos.config.MessagingConfig;
import com.memory.medicamentos.exceptions.NotFoundException;
import com.memory.medicamentos.models.Drug;
import com.memory.medicamentos.repositories.DrugRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class DrugServiceImpl implements DrugService {
    private final DrugRepository drugRepository;

    public DrugServiceImpl(DrugRepository drugRepository) {
        this.drugRepository = drugRepository;
    }

    @Override
    @Transactional
    public Drug save(Drug drug) {
        log.info("saving drug [{}]", drug.getName());
        return drugRepository.save(drug);
    }

    @Override
    public Page<Drug> getAll(Pageable paging) {
        log.info("retrieving all drugs");
        return drugRepository.findAll(paging);
    }

    @Override
    @Transactional
    public Drug update(Drug drug, Long drugToUpdateId) {
        Drug drugToUpdate = getDrugByIdOrElseThrow(drugToUpdateId);

        if (drug.getName() != null) drugToUpdate.setName(drug.getName());
        if (drug.getPrice() != null) drugToUpdate.setPrice(drug.getPrice());
        if (drug.getExpirationDate() != null) drugToUpdate.setExpirationDate(drug.getExpirationDate());
        if (drug.getTelSAC() != null) drugToUpdate.setTelSAC(drug.getTelSAC());
        if (drug.getAnvisaNumber() != null) drugToUpdate.setAnvisaNumber(drug.getAnvisaNumber());
        if (drug.getManufacturer() != null) drugToUpdate.setManufacturer(drug.getManufacturer());
        if (drug.getAdverseReactionIds() != null) drugToUpdate.setAdverseReactionIds(drug.getAdverseReactionIds());

        log.info("drug with id [{}] updated", drug.getId());
        return drugRepository.save(drugToUpdate);
    }

    private Drug getDrugByIdOrElseThrow(Long drugToUpdateId) {
        return drugRepository.findById(drugToUpdateId).orElseThrow(() -> {
            log.error("drug with id [{}] does not exists", drugToUpdateId);
            throw new NotFoundException();
        });
    }

    @Override
    @Transactional
    public void delete(Long drugId) {
        log.info("deleting drug with id [{}]", drugId);

        getDrugByIdOrElseThrow(drugId);
        drugRepository.deleteById(drugId);
    }

    @Override
    public Page<Drug> getAllByName(String name, Pageable paging) {
        log.info("retrieving all drugs by name");
        return drugRepository.findByName(name, paging);
    }

    @Override
    public Page<Drug> getAllByAnvisaNumber(String anvisaNumber, Pageable paging) {
        log.info("retrieving all drugs by anvisa number");
        return drugRepository.findByAnvisaNumber(anvisaNumber, paging);
    }

    @RabbitListener(queues = MessagingConfig.QUEUE)
    @Transactional
    public void consumeMessageFromQueue(Long adverseReactionId) {
        log.info("adverse reaction to be deleted: {}", adverseReactionId);
        drugRepository.removeAdverseReactionId(adverseReactionId);
    }
}
