package com.memory.medicamentos.services;

import com.memory.medicamentos.exceptions.NotFoundException;
import com.memory.medicamentos.models.Manufacturer;
import com.memory.medicamentos.repositories.ManufacturerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;

    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Manufacturer getById(Long manufacturerId) {
        return manufacturerRepository.findById(manufacturerId).orElseThrow(() -> { throw new NotFoundException(); });
    }

    @Override
    public Page<Manufacturer> getAll(Pageable paging) {
        log.info("retrieving all manufacturers");
        return manufacturerRepository.findAll(paging);
    }
}
