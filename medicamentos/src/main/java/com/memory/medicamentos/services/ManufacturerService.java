package com.memory.medicamentos.services;

import com.memory.medicamentos.models.Manufacturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManufacturerService {
    Manufacturer getById(Long manufacturerId);

    Page<Manufacturer> getAll(Pageable paging);
}
