package com.memory.medicamentos.services;

import com.memory.medicamentos.models.Drug;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.stream.DoubleStream;

public interface DrugService {
    Drug save(Drug drug);

    Page<Drug> getAll(Pageable paging);

    Drug update(Drug drug, Long drugToUpdateId);

    void delete(Long drugId);

    Page<Drug> getAllByName(String name, Pageable paging);

    Page<Drug> getAllByAnvisaNumber(String anvisaNumber, Pageable paging);
}
