package com.memory.medicamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MedicamentosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedicamentosApplication.class, args);
    }

}
