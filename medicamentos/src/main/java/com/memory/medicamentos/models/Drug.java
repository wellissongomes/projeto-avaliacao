package com.memory.medicamentos.models;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter @Setter
@Entity
@Table(name = "tb_drugs")
public class Drug {
    @Id
    @GeneratedValue
    private Long id;
    private String anvisaNumber;
    private String name;
    private Date expirationDate;
    private BigDecimal price;
    private String telSAC;

    @OneToOne
    private Manufacturer manufacturer;
    @ElementCollection
    @CollectionTable(name = "tb_drugs_adverse_reactions", joinColumns = @JoinColumn(name = "drug_id"))
    @Column(name = "adverse_reaction_id")
    private Set<Long> adverseReactionIds;
}
