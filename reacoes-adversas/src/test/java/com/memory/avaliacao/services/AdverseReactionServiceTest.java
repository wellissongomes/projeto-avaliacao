package com.memory.avaliacao.services;

import com.memory.avaliacao.models.AdverseReaction;
import com.memory.avaliacao.repositories.AdverseReactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.memory.avaliacao.config.MessagingConfig.EXCHANGE;
import static com.memory.avaliacao.config.MessagingConfig.ROUTING_KEY;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AdverseReactionServiceTest {
    @Mock
    private AdverseReactionRepository adverseReactionRepository;
    @Mock
    private RabbitTemplate rabbitTemplate;

    private AdverseReactionService adverseReactionService;
    private PodamFactory podamFactory;

    @BeforeEach
    void setUp() {
        podamFactory = new PodamFactoryImpl();
        adverseReactionService = new AdverseReactionImpl(adverseReactionRepository, rabbitTemplate);
    }

    @Test
    @DisplayName("Test save adverse reaction")
    public void testSave() {
        AdverseReaction adverseReaction = podamFactory.manufacturePojo(AdverseReaction.class);
        when(adverseReactionRepository.save(adverseReaction)).thenReturn(adverseReaction);

        AdverseReaction savedAdverseReaction = adverseReactionService.save(adverseReaction);

        assertEquals(adverseReaction, savedAdverseReaction);
        verify(adverseReactionRepository, times(1)).save(adverseReaction);
    }

    @Test
    @DisplayName("Test delete adverse reaction")
    public void testDelete() {
        Long adverseReactionId = 1L;
        when(adverseReactionRepository.existsById(adverseReactionId)).thenReturn(true);

        adverseReactionService.delete(adverseReactionId);

        verify(adverseReactionRepository, times(1)).deleteById(adverseReactionId);
        verify(rabbitTemplate, times(1)).convertAndSend(EXCHANGE, ROUTING_KEY, adverseReactionId);
    }


    @Test
    @DisplayName("Test update adverse reaction")
    public void testUpdate() {
        Long adverseReactionIdToUpdate = 1L;
        AdverseReaction adverseReaction = podamFactory.manufacturePojo(AdverseReaction.class);
        AdverseReaction adverseReactionToUpdate = podamFactory.manufacturePojo(AdverseReaction.class);
        adverseReactionToUpdate.setId(adverseReactionIdToUpdate);
        when(adverseReactionRepository.existsById(adverseReactionIdToUpdate)).thenReturn(true);
        when(adverseReactionRepository.findById(adverseReactionIdToUpdate)).thenReturn(Optional.of(adverseReactionToUpdate));
        when(adverseReactionRepository.save(adverseReactionToUpdate)).thenReturn(adverseReactionToUpdate);

        AdverseReaction updatedAdverseReaction = adverseReactionService.update(adverseReaction, adverseReactionIdToUpdate);

        assertEquals(adverseReaction.getDescription(), updatedAdverseReaction.getDescription());
        verify(adverseReactionRepository, times(1)).existsById(adverseReactionIdToUpdate);
        verify(adverseReactionRepository, times(1)).findById(adverseReactionIdToUpdate);
        verify(adverseReactionRepository, times(1)).save(adverseReactionToUpdate);
    }

    @Test
    @DisplayName("Test get all by ids")
    public void testGetByIdIn() {
        List<Long> ids = Arrays.asList(1L, 2L);
        Pageable pageable = PageRequest.of(0, 10);
        Page<AdverseReaction> expectedPage = new PageImpl<>(Collections.emptyList());
        when(adverseReactionRepository.findByIdIn(ids, pageable)).thenReturn(expectedPage);

        Page<AdverseReaction> resultPage = adverseReactionService.getByIdIn(ids, pageable);

        assertEquals(expectedPage, resultPage);
        verify(adverseReactionRepository, times(1)).findByIdIn(ids, pageable);
    }

}