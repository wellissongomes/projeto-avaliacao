package com.memory.avaliacao.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Builder
@Entity
@Table(name = "tb_adverse_reactions")
public class AdverseReaction {
    @Id
    @GeneratedValue
    private Long id;
    private String description;
}
