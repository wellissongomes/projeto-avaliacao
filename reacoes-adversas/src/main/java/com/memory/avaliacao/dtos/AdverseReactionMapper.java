package com.memory.avaliacao.dtos;

import com.memory.avaliacao.models.AdverseReaction;
import org.springframework.stereotype.Component;

@Component
public class AdverseReactionMapper {
    public AdverseReaction toAdverseReaction(AdverseReactionRequest adverseReactionRequest) {
        return AdverseReaction.builder()
                .description(adverseReactionRequest.getDescription())
                .build();
    }
}
