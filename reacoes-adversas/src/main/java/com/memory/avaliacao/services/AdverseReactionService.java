package com.memory.avaliacao.services;

import com.memory.avaliacao.models.AdverseReaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdverseReactionService {
    AdverseReaction save(AdverseReaction adverseReaction);

    void delete(Long adverseReactionId);

    AdverseReaction update(AdverseReaction adverseReaction, Long adverseReactionIdToUpdate);

    Page<AdverseReaction> getByIdIn(List<Long> ids, Pageable pageable);
}
