package com.memory.avaliacao.services;

import com.memory.avaliacao.config.MessagingConfig;
import com.memory.avaliacao.exceptions.NotFoundException;
import com.memory.avaliacao.models.AdverseReaction;
import com.memory.avaliacao.repositories.AdverseReactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;

import static com.memory.avaliacao.config.MessagingConfig.*;

@Slf4j
@Service
public class AdverseReactionImpl implements AdverseReactionService {
    private final AdverseReactionRepository adverseReactionRepository;
    private final RabbitTemplate rabbitTemplate;

    public AdverseReactionImpl(AdverseReactionRepository adverseReactionRepository, RabbitTemplate rabbitTemplate) {
        this.adverseReactionRepository = adverseReactionRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    @Transactional
    public AdverseReaction save(AdverseReaction adverseReaction) {
        log.info("saving adverse reaction with description: {}", adverseReaction.getDescription());
        return adverseReactionRepository.save(adverseReaction);
    }

    @Override
    @Transactional
    public void delete(Long adverseReactionId) {
        checkIfAdverseReactionExists(adverseReactionId);

        log.info("deleting adverse reaction with id: {}", adverseReactionId);
        adverseReactionRepository.deleteById(adverseReactionId);
        rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, adverseReactionId);
    }

    @Override
    @Transactional
    public AdverseReaction update(AdverseReaction adverseReaction, Long adverseReactionIdToUpdate) {
        checkIfAdverseReactionExists(adverseReactionIdToUpdate);

        var adverseReactionToUpdate = adverseReactionRepository.findById(adverseReactionIdToUpdate).get();
        log.info("updating adverse reaction with description [{}] to [{}]", adverseReactionToUpdate.getDescription(),
                                                                            adverseReaction.getDescription());

        adverseReactionToUpdate.setDescription(adverseReaction.getDescription());
        return adverseReactionRepository.save(adverseReactionToUpdate);
    }

    @Override
    public Page<AdverseReaction> getByIdIn(List<Long> ids, Pageable pageable) {
        log.info("retrieving adverse reactions with ids: {}", ids.toString());
        return adverseReactionRepository.findByIdIn(ids, pageable);
    }

    private void checkIfAdverseReactionExists(Long adverseReactionId) {
        adverseReactionExistsOrElseThrow(adverseReactionId, () -> {
            log.error("adverse reaction with id {} does not exists", adverseReactionId);
            throw new NotFoundException();
        });
    }

    private void adverseReactionExistsOrElseThrow(Long adverseReactionId, Supplier<? extends RuntimeException> supplier) {
        if (!adverseReactionRepository.existsById(adverseReactionId)) {
            supplier.get();
        }
    }
}
