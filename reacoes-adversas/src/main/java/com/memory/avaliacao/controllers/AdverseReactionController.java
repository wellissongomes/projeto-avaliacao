package com.memory.avaliacao.controllers;

import com.memory.avaliacao.dtos.AdverseReactionMapper;
import com.memory.avaliacao.dtos.AdverseReactionRequest;
import com.memory.avaliacao.dtos.PageResponse;
import com.memory.avaliacao.models.AdverseReaction;
import com.memory.avaliacao.services.AdverseReactionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/adverse-reactions")
public class AdverseReactionController {

    private final AdverseReactionService adverseReactionService;
    private final AdverseReactionMapper adverseReactionMapper;

    public AdverseReactionController(AdverseReactionService adverseReactionService, AdverseReactionMapper adverseReactionMapper) {
        this.adverseReactionService = adverseReactionService;
        this.adverseReactionMapper = adverseReactionMapper;
    }

    @GetMapping
    public ResponseEntity<PageResponse<AdverseReaction>> getAllAdverseReactionsFromIds(@RequestParam List<Long> ids,
                                                                               @RequestParam(defaultValue = "0") int page,
                                                                               @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<AdverseReaction> adverseReactionPage = adverseReactionService.getByIdIn(ids, paging);
        return new ResponseEntity<>(new PageResponse(adverseReactionPage), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AdverseReaction> addAdverseReaction(@RequestBody AdverseReactionRequest adverseReactionRequest) {
        var adverseReaction = adverseReactionMapper.toAdverseReaction(adverseReactionRequest);
        var savedAdverseReaction = adverseReactionService.save(adverseReaction);
        return new ResponseEntity<>(savedAdverseReaction, HttpStatus.CREATED);
    }

    @DeleteMapping("/{adverseReactionId}")
    public ResponseEntity<Void> removeAdverseReaction(@PathVariable Long adverseReactionId) {
        System.out.println(adverseReactionId);
        adverseReactionService.delete(adverseReactionId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{adverseReactionId}")
    public ResponseEntity<AdverseReaction> editAdverseReaction(@RequestBody AdverseReactionRequest adverseReactionRequest,
                                                               @PathVariable Long adverseReactionId) {
        var adverseReaction = adverseReactionMapper.toAdverseReaction(adverseReactionRequest);
        var updatedAdverseReaction = adverseReactionService.update(adverseReaction, adverseReactionId);
        return new ResponseEntity<>(updatedAdverseReaction, HttpStatus.OK);
    }

}
