package com.memory.avaliacao.repositories;

import com.memory.avaliacao.models.AdverseReaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface AdverseReactionRepository extends JpaRepository<AdverseReaction, Long> {
    Page<AdverseReaction> findByIdIn(Collection<Long> ids, Pageable pageable);
}
