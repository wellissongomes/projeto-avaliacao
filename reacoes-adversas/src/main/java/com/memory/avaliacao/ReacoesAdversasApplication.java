package com.memory.avaliacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ReacoesAdversasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReacoesAdversasApplication.class, args);
	}

}
